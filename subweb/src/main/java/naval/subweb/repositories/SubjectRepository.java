package naval.subweb.repositories;

import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import naval.subweb.domain.models.Subject;

@Repository
public interface SubjectRepository extends JpaRepository<Subject, UUID> {
}
