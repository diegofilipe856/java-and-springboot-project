package naval.subweb.repositories;

import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import naval.subweb.domain.models.Graduation;

@Repository
public interface GraduationRepository extends JpaRepository<Graduation, UUID>{
    
}
