package naval.subweb.repositories;

import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import naval.subweb.domain.models.Student;

@Repository
public interface StudentRepository extends JpaRepository<Student, UUID> {
    
}
