package naval.subweb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JsprojectApplication {

	public static void main(String[] args) {
		SpringApplication.run(JsprojectApplication.class, args);
	}

}
