package naval.subweb.config;

import java.util.List;
import java.util.LinkedList;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class ExceptionHandlerConfig {
    
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(RuntimeException.class)
    public String runtimeHandler(RuntimeException ex){
        return "Erro no runtime :/ ==>" + ex.getMessage();
    }


    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public List<String> handleValidation(MethodArgumentNotValidException ex) {
        final List<String> errors = new LinkedList<>();
        ex.getFieldErrors().forEach(error ->{
            errors.add(error.getDefaultMessage());
        });
    return errors;
    }
}
