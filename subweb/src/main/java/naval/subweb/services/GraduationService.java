package naval.subweb.services;

import java.util.LinkedList;
import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import lombok.RequiredArgsConstructor;
import naval.subweb.domain.builders.GraduationBuilder;
import naval.subweb.domain.dtos.GraduationDTO;
import naval.subweb.domain.inputs.GraduationInput;
import naval.subweb.domain.models.Graduation;
import naval.subweb.domain.models.Student;
import naval.subweb.domain.models.Teacher;
import naval.subweb.repositories.GraduationRepository;
import naval.subweb.repositories.TeacherRepository;
@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class GraduationService{
    private List<GraduationDTO> graduationList = new LinkedList<>();
    private final GraduationRepository repository;
    private final TeacherRepository teachrepository;

    public Graduation createGraduation(GraduationInput graduationInput){
        Graduation graduation = GraduationBuilder.build(graduationInput);
        repository.save(graduation);
        return graduation;
    }
    
    public List<Graduation> catchAllGraduation(){
        List<Graduation> allGraduations = repository.findAll();
        return allGraduations;
    }

    public Graduation catchOneGraduation(String graduationName){
        List<Graduation> allGraduations = repository.findAll();
        for (Graduation graduation: allGraduations) {
            if(graduation.getGraduationName().equals(graduationName)){
                return graduation;
            }
        }
        return null;
    }   

    public GraduationDTO changeCoordinator(UUID graduationId, UUID graduationCoordinator){
        Graduation graduation = repository.getReferenceById(graduationId);
        Teacher teacher = teachrepository.getReferenceById(graduationCoordinator);
        graduation.setGraduationTeacher(teacher);
        repository.save(graduation);
        return GraduationBuilder.build(graduation);
        
    } 
    public List<Student> findAllStudents(UUID id) {
        Graduation graduation = repository.getReferenceById(id);
        return graduation.getStudents();
    }
}

/* def manteiga(pão, faca):

    public Graduation create(Graduation graduation){
    método que retorna uma Graduation e tem como nome create     
        */