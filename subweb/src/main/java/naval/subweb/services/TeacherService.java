package naval.subweb.services;

import java.util.LinkedList;
import java.util.List;

import org.springframework.stereotype.Service;

import naval.subweb.domain.builders.TeacherBuilder;
import naval.subweb.domain.inputs.TeacherInput;
import naval.subweb.domain.models.Teacher;
import naval.subweb.repositories.TeacherRepository;
import org.springframework.beans.factory.annotation.Autowired;
import lombok.RequiredArgsConstructor;



@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class TeacherService {
    private List<Teacher> listOfTeachers = new LinkedList<>();
    private final TeacherRepository repository;

    public Teacher createTeacher(TeacherInput TeacherInput){
        Teacher teacher = TeacherBuilder.build(TeacherInput);
        repository.save(teacher);
        return teacher;
    }

    public List<Teacher> findAllTeachers(){
        return repository.findAll();
    }
    public Teacher findOneTeacher(String cpf) {
        List<Teacher> allTeachers = repository.findAll();
        for (Teacher Teacher: allTeachers){
            if (Teacher.getCpf().equals(cpf)){
                return Teacher;
            }
        }
        return null;
    }
}

