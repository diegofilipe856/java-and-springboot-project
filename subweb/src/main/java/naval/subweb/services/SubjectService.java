package naval.subweb.services;

import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import lombok.RequiredArgsConstructor;
import naval.subweb.domain.builders.SubjectBuilder;
import naval.subweb.domain.dtos.SubjectDTO;
import naval.subweb.domain.inputs.SubjectInput;
import naval.subweb.domain.models.Subject;
import naval.subweb.domain.models.Teacher;
import naval.subweb.repositories.SubjectRepository;
import naval.subweb.repositories.TeacherRepository;

@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class SubjectService {
    private final SubjectRepository repository;
    private final TeacherRepository teachrepository;

    public SubjectDTO createSubject(SubjectInput subjectInput){
        Subject subject = SubjectBuilder.build(subjectInput);
        repository.save(subject);
        return SubjectBuilder.build(subject);
    }

    public List<SubjectDTO> catchAll(){
        List<Subject> allSubjects = repository.findAll();
        return SubjectBuilder.build(allSubjects);
    }

    public SubjectDTO catchOneSubject(String subjectName) {
        List<Subject> allSubjects = repository.findAll();
        for (Subject subject:allSubjects){
            if(subject.getSubjectName().equals(subjectName)){
                return SubjectBuilder.build(subject);
            }
        }
        return null;
    }

    public SubjectDTO changeTeacher(UUID subjectId, UUID teacherId){
        Subject subject = repository.getReferenceById(subjectId);
        Teacher teacher = teachrepository.getReferenceById(teacherId);
        subject.setSubjectTeacher(teacher);
        repository.save(subject);
        return SubjectBuilder.build(subject);
    }

}
