package naval.subweb.services;


import naval.subweb.domain.builders.StudentBuilder;
import naval.subweb.domain.dtos.StudentDTO;
import naval.subweb.domain.inputs.StudentInput;
import naval.subweb.domain.models.Graduation;
import naval.subweb.domain.models.Student;
import naval.subweb.repositories.GraduationRepository;
import naval.subweb.repositories.StudentRepository;

import java.util.LinkedList;
import java.util.List;
import java.util.UUID;

import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;


@Slf4j
@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class StudentService {

    private final StudentRepository repository;
    private final GraduationRepository gradrepository;


    private List<Student> ListOfStudents = new LinkedList<>();
    
    public Student createStudent(StudentInput studentInput){
        /*ListOfStudents.add(student);
    */
    Student student = StudentBuilder.build(studentInput);
    Graduation graduation = gradrepository.getReferenceById(studentInput.getGraduationId());
    student.setGraduation(graduation);
    repository.save(student);
    
    return student;
    }

    public List<Student> findAll(){
        return repository.findAll();
    }

    public Student findOneStudent(String cpf) {
        List<Student> allStudents = repository.findAll();
        for (Student student:allStudents){
            if (student.getCpf().equals(cpf)){
                return student;
            }
        }
        return null;
    }

    public StudentDTO changeCourse(String cpf,UUID graduationId){
        List<Student> allStudents = repository.findAll();
        Graduation graduation = gradrepository.getReferenceById(graduationId);
        for (Student student:ListOfStudents){
            if (student.getCpf().equals(cpf)){
                student.setGraduation(graduation);
                repository.save(student);
                return StudentBuilder.build(student);
            }
        }
        return null;
    }

    public Graduation findStudentGraduation(UUID id) {
        Student student = repository.getReferenceById(id);
        Graduation graduation = student.getGraduation();
        return graduation;
    }


}

