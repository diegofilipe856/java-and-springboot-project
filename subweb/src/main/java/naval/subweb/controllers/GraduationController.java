package naval.subweb.controllers;

import java.util.List;
import java.util.UUID;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import naval.subweb.domain.dtos.GraduationDTO;
import naval.subweb.domain.dtos.StudentDTO;
import naval.subweb.domain.inputs.GraduationInput;
import naval.subweb.domain.models.Graduation;
import naval.subweb.domain.models.Student;
import naval.subweb.services.GraduationService;
import naval.subweb.domain.builders.GraduationBuilder;
import naval.subweb.domain.builders.StudentBuilder;
import lombok.RequiredArgsConstructor;

@Tag(name = "Graduation", description = "Endpoints das graduações.")
@RestController
@RequestMapping("/graduation")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class GraduationController {
    private final GraduationService service;

    @Operation(
        summary = "Criar uma nova graduação",
        description = "Cria uma nova graduação no banco de dados."
    )
    @PostMapping("/create")
    public GraduationDTO createGraduation(@Valid @RequestBody GraduationInput graduationInput){
        Graduation graduation = service.createGraduation(graduationInput);
        return GraduationBuilder.build(graduation);
    }

     @GetMapping("/all")
    public List<Graduation> catchAllGraduation(){
        return service.catchAllGraduation();
    }

    @GetMapping("/graduation-name/{graduationName}")
    public Graduation catchOneGraduation(@PathVariable String graduationName){
        return service.catchOneGraduation(graduationName);
    }

    @PutMapping("/change-coordinator/{coordinatorId}/{graduationId}")
    public GraduationDTO changeCoordinator(@PathVariable UUID graduationId, @PathVariable UUID coordinatorId){
        return service.changeCoordinator(graduationId, coordinatorId);
    }
    @GetMapping("/{id}/students")
    public List<StudentDTO> findAllStudents(@PathVariable UUID id) {
        List<Student> students = service.findAllStudents(id);
        return StudentBuilder.build(students);
    }

}

    