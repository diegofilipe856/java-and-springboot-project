package naval.subweb.controllers;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import naval.subweb.domain.builders.TeacherBuilder;
import naval.subweb.domain.dtos.TeacherDTO;
import naval.subweb.domain.inputs.TeacherInput;
import naval.subweb.domain.models.Teacher;
import naval.subweb.services.TeacherService;
import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping("/Teachers")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class TeacherController {
    private final TeacherService service;

    @PostMapping("/create")
    public TeacherDTO createTeacher(@Valid @RequestBody TeacherInput Teacherinput){
        Teacher teacher = service.createTeacher(Teacherinput);
        return TeacherBuilder.build(teacher);
    }
 
    @GetMapping("/all")
    public List<Teacher> findAllTeachers(){
        return service.findAllTeachers();
    }

    @GetMapping("/CPF/{cpf}")
    public Teacher findOne(@PathVariable String cpf){
        return service.findOneTeacher(cpf);
    }
}

