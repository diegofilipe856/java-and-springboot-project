package naval.subweb.controllers;

import java.util.List;
import java.util.UUID;

import javax.validation.Valid;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.v3.oas.annotations.tags.Tag;
import naval.subweb.domain.builders.GraduationBuilder;
import naval.subweb.domain.builders.StudentBuilder;
import naval.subweb.domain.dtos.StudentDTO;
import naval.subweb.domain.dtos.GraduationDTO;
import naval.subweb.domain.inputs.StudentInput;
import naval.subweb.domain.models.Graduation;
import naval.subweb.domain.models.Student;
import naval.subweb.services.StudentService;
import lombok.RequiredArgsConstructor;

@Tag(name="Students", description = "Endpoints dos estudantes")
@RestController
@RequestMapping("/students")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class StudentController {
    private final StudentService service;

    @PostMapping("/create")
    public StudentDTO createStudent(@Valid @RequestBody StudentInput studentInput){
        Student student = service.createStudent(studentInput);
        return StudentBuilder.build(student);
    }

    @GetMapping("/all")
    public List<Student> findAll(){
        return service.findAll();
    }

    @GetMapping("/CPF/{cpf}")
    public Student findOne(@PathVariable String cpf){
        return service.findOneStudent(cpf);
    }
 
    @PutMapping("/{cpf}/{graduationId}")
    public StudentDTO changeCourse(@PathVariable String cpf,@PathVariable UUID graduationId){
        return service.changeCourse(cpf,graduationId);
    }
 

    @GetMapping("/{id}/graduation")
    public GraduationDTO findStudentGraduation(@PathVariable UUID id) {
        Graduation graduation = service.findStudentGraduation(id);
        return GraduationBuilder.build(graduation);
    }
}

