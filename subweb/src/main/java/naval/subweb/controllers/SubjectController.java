package naval.subweb.controllers;

import java.util.List;
import java.util.UUID;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import naval.subweb.domain.builders.SubjectBuilder;
import naval.subweb.domain.dtos.SubjectDTO;
import naval.subweb.domain.inputs.SubjectInput;
import naval.subweb.domain.models.Subject;
import naval.subweb.services.SubjectService;
import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping("/subjects")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class SubjectController {
    private final SubjectService service;

    @PostMapping("/create")
    public SubjectDTO createSubject(@Valid @RequestBody SubjectInput subjectInput){
        return service.createSubject(subjectInput);
    }

  @GetMapping
    public List<SubjectDTO> catchAll(){
        return service.catchAll();
    }
 
    @GetMapping("/subject-name/{subjectName}")
    public SubjectDTO findOne(@PathVariable String subjectName){
        return service.catchOneSubject(subjectName);
    }

    @PutMapping("/change-teacher/{teacherId}/{subjectId}")
    public SubjectDTO changeTeacher(@PathVariable UUID subjectId, @PathVariable UUID teacherId){
        return service.changeTeacher(subjectId, teacherId);
    }
}

