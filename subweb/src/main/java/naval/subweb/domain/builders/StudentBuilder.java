package naval.subweb.domain.builders;

import java.util.LinkedList;
import java.util.List;

import naval.subweb.domain.dtos.StudentDTO;
import naval.subweb.domain.inputs.StudentInput;
import naval.subweb.domain.models.Student;

public class StudentBuilder {

    public static Student build(StudentInput input){
        return Student.builder()
        .age(input.getAge())
        .name(input.getName())
        .cpf(input.getCpf())
        .Birthday(input.getBirthday())
        .rg(input.getRg())
        .dispatchingAgency(input.getDispatchingAgency())
        .build();
    }

    public static StudentDTO build(Student student) {
        return StudentDTO.builder()
                .studentId(student.getId())
                .age(student.getAge())
                .cpf(student.getCpf())
                .studentName(student.getName())
                .build();
    }
    public static List<StudentDTO> build(List<Student> students) {
        List<StudentDTO> finalList = new LinkedList<>();
        for (Student student : students) {
            StudentDTO studentDTO = build(student);
            finalList.add(studentDTO);
        }
        return finalList;
    }
    
}
