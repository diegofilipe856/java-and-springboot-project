package naval.subweb.domain.builders;

import java.util.LinkedList;
import java.util.List;

import naval.subweb.domain.dtos.SubjectDTO;
import naval.subweb.domain.inputs.SubjectInput;
import naval.subweb.domain.models.Subject;

public class SubjectBuilder {
    public static Subject build(SubjectInput input){
        return Subject.builder()
        .subjectName(input.getSubjectName())
        .code(input.getCode())
        .description(input.getDescription())
        /* .teacherId(input.getTeacherId())*/
        .build();
    }

    public static SubjectDTO build(Subject Subject) {
        return SubjectDTO.builder()
            .subjectName(Subject.getSubjectName())
            .code(Subject.getCode())
            .description(Subject.getDescription())
            /*.teacherId(Subject.getTeacherId())*/
            .build();
    }
    public static List<SubjectDTO> build(List<Subject> Subjects) {
        List<SubjectDTO> finalList = new LinkedList<>();
        for (Subject Subject : Subjects) {
            SubjectDTO SubjectDTO = build(Subject);
            finalList.add(SubjectDTO);
        }
        return finalList;
    }
    
}
