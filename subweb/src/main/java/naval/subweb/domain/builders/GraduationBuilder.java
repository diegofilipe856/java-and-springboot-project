package naval.subweb.domain.builders;

import naval.subweb.domain.dtos.GraduationDTO;
import naval.subweb.domain.inputs.GraduationInput;
import naval.subweb.domain.models.Graduation;

public class GraduationBuilder {
    
    public static Graduation build(GraduationInput input){
        return Graduation.builder()
        .graduationName(input.getGraduationName())
        .dateOfCreation(input.getDateOfCreation())
        .buildingName(input.getBuildingName())
        .coordinatorId(input.getCoordinatorId())
        .build();
    }

    public static GraduationDTO build(Graduation Graduation) {
        return GraduationDTO.builder()
            .graduationName(Graduation.getGraduationName())
            .dateOfCreation(Graduation.getDateOfCreation())
            .buildingName(Graduation.getBuildingName())
            .coordinatorId(Graduation.getCoordinatorId())
            .build();
    }

}
