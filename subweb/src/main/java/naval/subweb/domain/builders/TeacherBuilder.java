package naval.subweb.domain.builders;

import naval.subweb.domain.dtos.TeacherDTO;
import naval.subweb.domain.inputs.TeacherInput;
import naval.subweb.domain.models.Teacher;

public class TeacherBuilder {
    public static Teacher build(TeacherInput input){
        return Teacher.builder()
        .name(input.getTeacherName())
        .cpf(input.getCpf())
        .titulation(input.getTitulation())
        .build();
    }

    public static TeacherDTO build(Teacher Teacher) {
        return TeacherDTO.builder()
                .cpf(Teacher.getCpf())
                .teacherName(Teacher.getName())
                .titulation(Teacher.getTitulation())
                .build();
    }
    
}

