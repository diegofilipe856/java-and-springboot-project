package naval.subweb.domain.dtos;

import javax.validation.constraints.NotBlank;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Schema(description = "Representa uma grauduação")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class GraduationDTO {
    @Schema(description = "ID da graduação")
    private String graduationId;

    @Schema(description = "Nome da graduação")
    @NotBlank(message = "A graduação precisa ter um nome!")
    private String graduationName;
    
    @Schema(description = "Data de criação")
    private String dateOfCreation;

    @Schema(description = "Prédio na qual o curso é dado")
    private String buildingName;

    @Schema(description = "UUID do professor coordenador do curso")
    @NotBlank(message = "O curso deve ter um coordenador responsável!")
    private String coordinatorId;
}   