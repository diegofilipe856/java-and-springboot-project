package naval.subweb.domain.dtos;


import java.util.UUID;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class StudentDTO {
    private UUID studentId;
    private String cpf;
    private String studentName;
    private Integer age;
    private String birthday;
    private String rg;
    private String dispatchingAgency;
    private String graduationId;
}   