package naval.subweb.domain.inputs;

import javax.validation.constraints.NotBlank;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;


@Data
@Schema(description = "Representação da entrada de graduações")
public class GraduationInput {

    @Schema(description = "Nome da graduação")
    @NotBlank(message = "A graduação precisa ter um nome!")
    private String graduationName;
    
    @Schema(description = "Data de criação")
    private String dateOfCreation;

    @Schema(description = "Prédio na qual o curso é dado")
    private String buildingName;

    @Schema(description = "UUID do professor coordenador do curso")
    @NotBlank(message = "O curso deve ter um coordenador responsável!")
    private String coordinatorId;
}   

