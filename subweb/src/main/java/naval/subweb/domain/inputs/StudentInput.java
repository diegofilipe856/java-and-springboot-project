package naval.subweb.domain.inputs;

import java.util.UUID;

import javax.validation.constraints.NotBlank;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

@Data
@Schema(description = "Representação da entrada de estudantes")
public class StudentInput {
    
    @Schema(description = "Nome do estudante")
    @NotBlank(message = "Nome não pode ser vazio!")
    private String name;

    @Schema(description = "CPF do estudante")
    @NotBlank(message = "CPF não pode ser vazio!")
    private String cpf;

    @Schema(description = "Idade do estudante")
    private Integer age;

    @Schema(description = "RG do estudante")
    @NotBlank(message = "RG não pode ser vazio!")
    private String rg;

    @Schema(description = "Orgão expeditor")
    private String dispatchingAgency;

    @Schema(description = "Data de nascimento do estudante")
    private String Birthday;

    @Schema(description = "ID do curso de graduação do estudante")
    private UUID graduationId;
}
