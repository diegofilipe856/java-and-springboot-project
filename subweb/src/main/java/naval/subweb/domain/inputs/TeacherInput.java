package naval.subweb.domain.inputs;

import lombok.Data;


@Data
public class TeacherInput {
    private String cpf;
    private String teacherName;
    private String titulation;
}   