package naval.subweb.domain.inputs;

import java.util.UUID;

import lombok.Data;

@Data
public class SubjectInput {
    public String subjectName;
    public String code;
    public String description;
    /*public UUID teacherId;*/
    
}
