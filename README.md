examples of requests:
------------------------------------------------------------------
POST http://localhost:8080/graduation/create
Content-Type: application/json

{
    "graduationId": 1,
    "graduationName": "Ciencia da computacao",
    "dateOfCreation": "15/01/2002",
    "buildingName": "IC",
    "coordinatorId": "454f6d5sf5sd4"

}
----------------------------------------------------------------------
POST http://localhost:8080/students/create
Content-Type: application/json

{
    "studentId":"55451",
    "cpf":"123.456.789-99",
    "studentName":"Daigo",
    "birthday":"15/05",
    "rg":"54554456",
    "dispatchingAgency":"sds",
    "graduationId":"544545465454"
    }

----------------------------------------------------------------------

GET http://localhost:8080/students/all

GET http://localhost:8080/graduation/all


